package top.shiyiri.spring5.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Aunean
 * @date 2022/2/17 22:25
 */
@Configuration
@ComponentScan(basePackages = {"top.shiyiri.spring5"})
//开启AspectJ的自动代理
@EnableAspectJAutoProxy(proxyTargetClass = true)  //<aop:aspectj-autoproxy />
public class ConfigAop {
}
