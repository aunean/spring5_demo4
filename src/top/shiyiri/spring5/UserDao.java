package top.shiyiri.spring5;

/**
 * @author Aunean
 * @date 2022/2/17 18:10
 */
public interface UserDao {

    int add(int a, int b);

    String update(String id);
}
