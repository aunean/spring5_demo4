package top.shiyiri.spring5;

/**
 * @author Aunean
 * @date 2022/2/17 18:11
 */
public class UserDaoImpl implements UserDao {
    @Override
    public int add(int a, int b) {
        System.out.println("add方法执行");
        return a + b;
    }

    @Override
    public String update(String id) {
        return id;
    }
}
