package top.shiyiri.spring5.aopanno;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Aunean
 * @date 2022/2/17 22:01
 */
@Component
@Aspect
@Order(1) //在增强类上面添加注解 @Order(数字类型值)，数字类型值越小优先级越高
public class PersonProxy {

    //前置通知
    @Before(value = "execution(* top.shiyiri.spring5.aopanno.User.add(..))")
    public void before() {
        System.out.println("Person Before...");
    }
}
