package top.shiyiri.spring5.aopanno;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Aunean
 * @date 2022/2/17 21:39
 */
//增强的类
@Component
@Aspect //生成代理对象（定义为切面）
@Order(2)
public class UserProxy {

    //相同切入点抽取
    @Pointcut(value = "execution(* top.shiyiri.spring5.aopanno.User.add(..))")
    public void pointcut() {}

    //前置通知
    //@Before 注解表示作为前置通知
    @Before(value = "pointcut()")
    public void before() {
        System.out.println("before.....");
    }

    //后置通知（返回通知）
    @AfterReturning(value = "pointcut()")
    public void afterReturning() {
        System.out.println("afterReturning.....");
    }

    //最终通知
    @After(value = "pointcut()")
    public void after() {
        System.out.println("after.....");
    }

    //异常通知
    @AfterThrowing(value = "pointcut()")
    public void afterThrowing() {
        System.out.println("afterThrowing.....");
    }

    //环绕通知
    @Around(value = "pointcut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕之前.....");

        //被增强的方法执行
        joinPoint.proceed();

        System.out.println("环绕之后.....");
    }

}
