package top.shiyiri.spring5.aopanno;

import org.springframework.stereotype.Component;

/**
 * @author Aunean
 * @date 2022/2/17 21:38
 */

//被增强的类
@Component
public class User {
    public void add() {
//        int i = 1 / 0;
        System.out.println("add.......");
    }
}
